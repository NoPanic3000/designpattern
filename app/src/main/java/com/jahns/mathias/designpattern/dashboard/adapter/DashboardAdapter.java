package com.jahns.mathias.designpattern.dashboard.adapter;

import android.app.Activity;
import android.view.View;

import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter;
import com.jahns.mathias.designpattern.dashboard.model.Pattern;

import java.util.List;

/**
 * Created by dreit on 25.02.2018.
 */

public class DashboardAdapter extends ListDelegationAdapter<List<Pattern>> {

    public DashboardAdapter(Activity activity, List<Pattern> items, View.OnClickListener listener) {
        delegatesManager.addDelegate(new PatternCardAdapterDelegate(activity, listener));
        setItems(items);
    }
}
