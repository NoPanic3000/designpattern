package com.jahns.mathias.designpattern.pattern.factorymethod;

/**
 * Created by dreit on 03.03.2018.
 */

public class ConcreteProductBB extends Product {

    public ConcreteProductBB(){
        name = "ConcreteProductBB";
    }

    @Override
    public int getPrice() {
        return 400;
    }
}