package com.jahns.mathias.designpattern.dashboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.jahns.mathias.designpattern.MainActivity;
import com.jahns.mathias.designpattern.R;
import com.jahns.mathias.designpattern.dashboard.utils.LogUtil;

/**
 * Created by dreit on 25.02.2018.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private TextView tvName;
    private TextView tvLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_design_pattern);

        init();

        executePattern();
        showLog();
    }

    private void init() {
        tvName = findViewById(R.id.tv_pattern_name);
        tvLog = findViewById(R.id.tv_log);

        Intent intent = getIntent();
        String patternName = intent.getStringExtra(MainActivity.KEY_PATTERN_NAME);
        tvName.setText(patternName);
    }

    public abstract void executePattern();

    public void showLog() {
        tvLog.setText(LogUtil.getLogText());
        LogUtil.resetLog();
    }
}
