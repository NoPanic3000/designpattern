package com.jahns.mathias.designpattern.pattern.strategy;

import android.os.Bundle;

import com.jahns.mathias.designpattern.R;
import com.jahns.mathias.designpattern.dashboard.activity.BaseActivity;

/**
 * Created by dreit on 25.02.2018.
 */

public class ActivityStrategy extends BaseActivity {

    @Override
    public void executePattern() {
        Context context = new Context();
        context.execute();

        context.setStrategy(new ConcreteStrategyB());
        context.execute();
    }
}
