package com.jahns.mathias.designpattern.pattern.decorator.component;

/**
 * Created by mathiasjahns on 15.11.18.
 */

/* Component */
public abstract class Drink {

    protected String description = "unknown drink";

    public String getDescription(){
        return description;
    }

    public abstract double getPrice();
}
