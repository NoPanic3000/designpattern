package com.jahns.mathias.designpattern.dashboard.utils;

import android.util.Log;

import com.jahns.mathias.designpattern.pattern.strategy.ConcreteStrategyB;

/**
 * Created by dreit on 25.02.2018.
 */

public class LogUtil {

    private static String logText = "";

    public static void log(String newLog) {
        Log.d(LogUtil.class.getSimpleName(), newLog);
        logText = logText + "\n" + newLog;
    }

    public static String getLogText() {
        return logText;
    }

    public static void resetLog(){
        logText = "";
    }
}
