package com.jahns.mathias.designpattern.pattern.decorator.decorator;

import com.jahns.mathias.designpattern.pattern.decorator.component.Drink;

/**
 * Created by mathiasjahns on 15.11.18.
 */

/* ConcreteDecorator */
public class Milk extends Decorator {

    Drink drink;

    public Milk(Drink drink) {
        this.drink = drink;
    }

    @Override
    public String getDescription() {
        return drink.getDescription() + ", Milk";
    }

    @Override
    public double getPrice() {
        return 0.15 + drink.getPrice();
    }
}
