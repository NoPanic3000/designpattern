package com.jahns.mathias.designpattern.pattern.factorymethod;

import static com.jahns.mathias.designpattern.pattern.factorymethod.Product.TYP_B;
import static com.jahns.mathias.designpattern.pattern.factorymethod.Product.TYP_A;

/**
 * Created by dreit on 03.03.2018.
 */

public class ConcreteCreatorA extends Creator {

    @Override
    protected Product factoryMethod(String typ) {
        Product product = null;

        if (typ.equals(TYP_A)) {
            product = new ConcreteProductAA();
        } else if (typ.equals(TYP_B)) {
            product = new ConcreteProductAB();
        }
        return product;
    }
}
