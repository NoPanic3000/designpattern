package com.jahns.mathias.designpattern.pattern.decorator.decorator;

import com.jahns.mathias.designpattern.pattern.decorator.component.Drink;

/**
 * Created by mathiasjahns on 15.11.18.
 */
public abstract class Decorator extends Drink {

    @Override
    public abstract String getDescription();
}
