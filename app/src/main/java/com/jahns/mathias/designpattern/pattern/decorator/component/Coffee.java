package com.jahns.mathias.designpattern.pattern.decorator.component;

/**
 * Created by mathiasjahns on 15.11.18.
 */

/* ConcreteComponent */
public class Coffee extends Drink {

    public Coffee() {
        description = "Coffee";
    }

    @Override
    public double getPrice() {
        return 1.49;
    }
}
