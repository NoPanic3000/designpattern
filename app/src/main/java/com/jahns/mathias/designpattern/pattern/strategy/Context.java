package com.jahns.mathias.designpattern.pattern.strategy;

/**
 * Created by dreit on 25.02.2018.
 */

public class Context {

    /*
     * Instance variable for Strategy (Composition)
     * Of the type of interface -> Strategy
     * Default: ConcreteStrategyA
     */
    private Strategy strategy = new ConcreteStrategyA();

    public void execute() {
        // delegates behavior to strategy object
        strategy.executeAlgorithm();
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public Strategy getStrategy() {
        return strategy;
    }
}
