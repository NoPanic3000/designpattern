package com.jahns.mathias.designpattern.pattern.factorymethod;

/**
 * Created by dreit on 03.03.2018.
 */

public abstract class Creator {

    public Product createProduct(String typ){
        // brings concrete product, do not know which
        Product product = factoryMethod(typ);

        product.setState(42);
        product.prepare();

        return product;
    }

    protected abstract Product factoryMethod(String typ);
}
