package com.jahns.mathias.designpattern.pattern.factorymethod;

import com.jahns.mathias.designpattern.dashboard.activity.BaseActivity;

import static com.jahns.mathias.designpattern.dashboard.utils.LogUtil.log;
import static com.jahns.mathias.designpattern.pattern.factorymethod.Product.TYP_B;
import static com.jahns.mathias.designpattern.pattern.factorymethod.Product.TYP_A;

/**
 * Created by dreit on 03.03.2018.
 */

public class ActivityFactoryMethod extends BaseActivity {

    @Override
    public void executePattern() {
        ConcreteCreatorA concreteCreatorA = new ConcreteCreatorA();
        ConcreteCreatorB concreteCreatorB = new ConcreteCreatorB();

        Product productAA = concreteCreatorA.createProduct(TYP_A);
        log("productAA: " + productAA.getPrice());

        Product productBB = concreteCreatorB.createProduct(TYP_B);
        log("productBB: " + productBB.getPrice());
    }
}
