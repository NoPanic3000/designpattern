package com.jahns.mathias.designpattern.dashboard.model;

/**
 * Created by mathias.jahns on 25.02.2018.
 */

public class Pattern {
    private String name;
    private int id;

    public Pattern(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId(){
        return id;
    }
}
