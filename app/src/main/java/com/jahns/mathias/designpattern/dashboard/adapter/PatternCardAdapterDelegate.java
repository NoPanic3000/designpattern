package com.jahns.mathias.designpattern.dashboard.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;
import com.jahns.mathias.designpattern.R;
import com.jahns.mathias.designpattern.dashboard.model.Pattern;
import com.jahns.mathias.designpattern.dashboard.model.PatternCard;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.WeakHashMap;


/**
 * Created by dreit on 25.02.2018.
 */

public class PatternCardAdapterDelegate extends AdapterDelegate<List<Pattern>> {

    private LayoutInflater inflater;
    private View.OnClickListener listener;

    public PatternCardAdapterDelegate(Activity activity, View.OnClickListener listener) {
        inflater = activity.getLayoutInflater();
        this.listener = listener;
    }

    @Override
    protected boolean isForViewType(@NonNull List<Pattern> items, int position) {
        return items.get(position) instanceof PatternCard;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new PatternCardViewHolder(inflater.inflate(R.layout.item_pattern_card, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull List<Pattern> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
        PatternCardViewHolder vh = (PatternCardViewHolder) holder;

        PatternCard patternCard = (PatternCard) items.get(position);

        vh.button.setTag(patternCard.getId());
        vh.button.setText(patternCard.getName());
        vh.button.setOnClickListener(listener);
    }

    private class PatternCardViewHolder extends RecyclerView.ViewHolder {

        public Button button;

        public PatternCardViewHolder(View intemView) {
            super(intemView);
            button = intemView.findViewById(R.id.pattern_card_button);
        }
    }

    public interface PatternCardOnClicklistener{
        public void onItemClick();
    }
}
