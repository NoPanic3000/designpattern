package com.jahns.mathias.designpattern.pattern.factorymethod;

/**
 * Created by dreit on 03.03.2018.
 */

public class ConcreteProductBA extends Product {

    public ConcreteProductBA(){
        name = "ConcreteProductBA";
    }

    @Override
    public int getPrice() {
        return 300;
    }
}