package com.jahns.mathias.designpattern.pattern.factorymethod;

import static com.jahns.mathias.designpattern.dashboard.utils.LogUtil.log;

/**
 * Created by dreit on 03.03.2018.
 */

public abstract class Product {

    public final static String TYP_A = "TYP_A";
    public final static String TYP_B = "TYP_B";

    private int basisState;
    protected String name;

    public void setState(int state) {
        basisState = state;
        log("basisState: " + basisState);
    }

    public int getState() {
        return basisState;
    }

    public void prepare() {
        log("prepare: " + name);
    }

    public abstract int getPrice();
}
