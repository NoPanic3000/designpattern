package com.jahns.mathias.designpattern.pattern.strategy;

import android.util.Log;

import com.jahns.mathias.designpattern.dashboard.utils.LogUtil;

import static com.jahns.mathias.designpattern.dashboard.utils.LogUtil.log;


/**
 * Created by dreit on 25.02.2018.
 */

public class ConcreteStrategyA implements Strategy {

    @Override
    public void executeAlgorithm() {
        log("Concrete Strategy A");
    }
}
