package com.jahns.mathias.designpattern;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.jahns.mathias.designpattern.dashboard.adapter.DashboardAdapter;
import com.jahns.mathias.designpattern.dashboard.model.Pattern;
import com.jahns.mathias.designpattern.dashboard.model.PatternCard;
import com.jahns.mathias.designpattern.pattern.decorator.ActivityDecorator;
import com.jahns.mathias.designpattern.pattern.factorymethod.ActivityFactoryMethod;
import com.jahns.mathias.designpattern.pattern.strategy.ActivityStrategy;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public final static String KEY_PATTERN_NAME = "PATTERN_NAME";
    public final static String KEY_PATTERN_ID = "PATTERN_ID";

    public final static int PATTERN_ID_STRTEGY = 112;
    public final static int PATTERN_ID_FACTORY_METHOD = 113;
    public final static int PATTERN_ID_DECORATOR = 114;

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        DashboardAdapter dashboardAdapter = new DashboardAdapter(this, getItems(), this::onItemClick);
        recyclerView.setAdapter(dashboardAdapter);
    }

    private void onItemClick(View v) {
        int id = Integer.parseInt(v.getTag().toString());
        String name;
        Intent intent = null;

        switch (id) {
            case PATTERN_ID_STRTEGY:
                name = getString(R.string.pattern_name_strategy);
                intent = new Intent(this, ActivityStrategy.class);
                break;
            case PATTERN_ID_FACTORY_METHOD:
                name = getString(R.string.pattern_name_factory_method);
                intent = new Intent(this, ActivityFactoryMethod.class);
                break;
            case PATTERN_ID_DECORATOR:
                name = getString(R.string.pattern_name_decorator);
                intent = new Intent(this, ActivityDecorator.class);
                break;
            default:
                name = "empty";
        }

        if(intent != null) {
            intent.putExtra(KEY_PATTERN_NAME, name);
            intent.putExtra(KEY_PATTERN_ID, id);
            startActivity(intent);
        }
    }

    public List<Pattern> getItems() {
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(new PatternCard(getString(R.string.pattern_name_strategy), PATTERN_ID_STRTEGY));
        patternList.add(new PatternCard(getString(R.string.pattern_name_factory_method), PATTERN_ID_FACTORY_METHOD));
        patternList.add(new PatternCard(getString(R.string.pattern_name_decorator), PATTERN_ID_DECORATOR));
        return patternList;
    }
}
