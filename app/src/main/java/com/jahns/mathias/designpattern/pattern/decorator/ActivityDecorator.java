package com.jahns.mathias.designpattern.pattern.decorator;

import com.jahns.mathias.designpattern.dashboard.activity.BaseActivity;
import com.jahns.mathias.designpattern.pattern.decorator.component.Coffee;
import com.jahns.mathias.designpattern.pattern.decorator.component.Drink;
import com.jahns.mathias.designpattern.pattern.decorator.component.Espresso;
import com.jahns.mathias.designpattern.pattern.decorator.decorator.Chocolate;
import com.jahns.mathias.designpattern.pattern.decorator.decorator.Milk;

import static com.jahns.mathias.designpattern.dashboard.utils.LogUtil.log;

/**
 * Created by mathiasjahns on 15.11.18.
 */
public class ActivityDecorator extends BaseActivity {

    @Override
    public void executePattern() {
        Drink espresso = new Espresso();
        log(espresso.getDescription() + " " + espresso.getPrice() + " €");
        // Espresso 1.99 €

        espresso = new Chocolate(espresso);
        log(espresso.getDescription() + " " + espresso.getPrice() + " €");
        // Espresso, Chocolate 2.19 €

        espresso = new Milk(espresso);
        log(espresso.getDescription() + " " + espresso.getPrice() + " €");
        // Espresso, Chocolate, Milk 2.34 €

        Drink coffee = new Coffee();
        log(coffee.getDescription() + " " + coffee.getPrice() + " €");
        // Coffee 1.49 €

        coffee = new Milk(coffee);
        log(coffee.getDescription() + " " + coffee.getPrice() + " €");
        // Coffee, Milk 1.64 €
    }
}

