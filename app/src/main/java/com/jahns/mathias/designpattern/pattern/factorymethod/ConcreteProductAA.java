package com.jahns.mathias.designpattern.pattern.factorymethod;

/**
 * Created by dreit on 03.03.2018.
 */

public class ConcreteProductAA extends Product {

    public ConcreteProductAA(){
        name = "ConcreteProductAA";
    }

    @Override
    public int getPrice() {
        return 100;
    }
}
