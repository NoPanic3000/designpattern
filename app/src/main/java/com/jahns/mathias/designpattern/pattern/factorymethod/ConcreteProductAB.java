package com.jahns.mathias.designpattern.pattern.factorymethod;

/**
 * Created by dreit on 03.03.2018.
 */

public class ConcreteProductAB extends Product {

    public ConcreteProductAB(){
        name = "ConcreteProductAB";
    }

    @Override
    public int getPrice() {
        return 200;
    }
}