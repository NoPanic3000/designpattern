package com.jahns.mathias.designpattern.pattern.decorator.decorator;

import com.jahns.mathias.designpattern.pattern.decorator.component.Drink;

/**
 * Created by mathiasjahns on 15.11.18.
 */

/* ConcreteDecorator */
public class Chocolate extends Decorator {

    private Drink drink;

    public Chocolate(Drink drink) {
        this.drink = drink;
    }

    @Override
    public String getDescription() {
        return drink.getDescription() + ", Chocolate";
    }

    @Override
    public double getPrice() {
        return 0.20 + drink.getPrice();
    }
}
