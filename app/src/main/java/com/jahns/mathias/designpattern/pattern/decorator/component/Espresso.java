package com.jahns.mathias.designpattern.pattern.decorator.component;

/**
 * Created by mathiasjahns on 15.11.18.
 */

/* ConcreteComponent */
public class Espresso extends Drink {

    public Espresso() {
        description = "Espresso";
    }

    @Override
    public double getPrice() {
        return 1.99;
    }
}
