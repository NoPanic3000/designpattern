package com.jahns.mathias.designpattern.pattern.strategy;

/**
 * Created by dreit on 25.02.2018.
 */

interface Strategy {

    public void executeAlgorithm();

}
